<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'PublicController@home');
Route::get('/product/{id}', 'PublicController@showProduct');


//oerder create
Route::post('/create', 'PublicController@store');


Route::get('/register','RegistrationController@create');
Route::post('/register','RegistrationController@store');

Route::get('/login','SessionsController@create')->name('login');
Route::post('/login','SessionsController@store');

Route::get('/logout','SessionsController@destroy');


Route::group(['middleware' => 'auth','prefix'=>'admin'], function(){

    Route::get('/', function () {
        return view('admin.index');
    });

    Route::get('/product', 'ProductController@index');
    Route::get('/product/{id}/show', 'ProductController@show');

//This Route For comments
    Route::get('/comments/{id}','CommentsController@show');

    Route::get('/products','productsController@index');
    Route::get('/products/create','productsController@create');
    Route::post('/products','productsController@store');
    Route::patch('/products/update/{id}','productsController@update');
    Route::get('/products/{post}','productsController@show');
    Route::get('/products/{product}/edit','productsController@edit');
    Route::patch('/products/{product}','productsController@update');

    Route::get('/products','ProductsController@index');
    Route::get('/products/create','ProductsController@create');
    Route::post('/products','ProductsController@store');
    Route::get('/products/{id}','ProductsController@show');
    Route::delete('/products/{product}','ProductsController@destroy');
    Route::post('/products/comments','ProductsController@addComment');
//tags
    Route::get('/tags/create','TagsController@create');
    Route::get('/tags','TagsController@index');
    Route::post('/tags','TagsController@store');
    Route::get('/tags/{tag}','TagsController@show');
    Route::get('/tags/{tag}/edit','TagsController@edit');
    Route::patch('/tags/{tag}','TagsController@update');
    Route::delete('/tags/{tag}','TagsController@destroy');

    Route::resource('/categories','CategoriesController');

    Route::resource('/suppliers','SuppliersController');

    //Sliders
    Route::get('/sliders','SlidersController@index');
    Route::get('/sliders/create','SlidersController@create');
    Route::post('/sliders','SlidersController@store');
    Route::get('/sliders/{slider}','SlidersController@show');
    Route::delete('/sliders/{slider}','SlidersController@destroy');
    Route::get('/sliders/{id}/edit/','SlidersController@edit');
    Route::patch('/sliders/{slider}','SlidersController@update');

    //    orders show route

    Route::get('/show','OrdersController@index');

    //    delete order
    Route::delete ('/oreder/delete/{id}','OrdersController@destroy');
});

Route::Post('/like/store/{id}', 'LikeController@store');
Route::Post('/comment/{id}', 'LikeController@store');



