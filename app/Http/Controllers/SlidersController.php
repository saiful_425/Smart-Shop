<?php

namespace App\Http\Controllers;

use App\Slider;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const UPLOAD_DIR = '/uploads/sliders/';

    public function index()
    {
        $sliders = Slider::all();
        return view('admin.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sliderData = $request->only('image');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $sliderData['image'] = $this->uploadImage($file);
        } else {
            $sliderData['image'] = null;
        }

        $slider = Slider::create($sliderData);


        session()->flash('message','Slider Inserted Successfully ');
        return redirect('/admin/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('admin.sliders.show',compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $slider = Slider::find($id);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $sliderData['image'] = $this->uploadImage($file);
        } else {
            $sliderData['image'] = $slider->image;
        }

        $slider->update($sliderData);
        

        session()->flash('message','Slider Updated Successfully ');
        return redirect('/admin/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)//delete ok
    {
        $this->unlinkImage($slider->image);
        $slider->delete();
        session()->flash('message','Slider Deleted Successfully ');
        return redirect('/admin/sliders');
    }

    private function uploadImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();

        Image::make($file)->resize(1000, 591)->save(public_path() . self::UPLOAD_DIR . $image_file_name);

        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }

    private function unlinkImage($img)
    {
        if ($img != '' && file_exists(public_path() . self::UPLOAD_DIR . $img)) {
            @unlink(public_path() . self::UPLOAD_DIR . $img);
        }
    }
}
