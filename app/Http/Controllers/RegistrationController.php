<?php

namespace App\Http\Controllers;

use App\Mail\Welcome;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }
    public function store(Request $request)
    {
        //dd($request);
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $data['name'] = request('name');
        $data['email'] = request('email');
        $data['password'] =bcrypt(request('password')) ;

       $user =  User::create($data);

        auth()->login($user);
        \Mail::to($user)->send(new Welcome($user));

        return redirect('/admin');

    }
}
