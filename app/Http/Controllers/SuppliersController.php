<?php

namespace App\Http\Controllers;

use App\Notifications\SupplierAdd;
use App\Supplier;
use App\User;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{  
    /**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    const UPLOAD_DIR = '/uploads/suppliers/logo/';

    public function index()
    {
        $suppliers = Supplier::all();

        return view('admin.suppliers.index',compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatedData = $request->validate([
            'company_name' => 'required|min:5',
            'email' => 'required|email',
            'address' => 'required|min:5',
        ]);

        if ($request->hasFile('logo')) {
            //  $this->unlinkImage($article->image);
            $file = $request->file('logo');
            $validatedData['logo'] = $this->uploadImage($file);
        } else {
            $productData['logo'] = null;
        }

        $supplier = Supplier::create($validatedData);


        User::all()->each(function($user) use ($supplier){
            $user->notify(new SupplierAdd($supplier));
        });


        session()->flash('message','Supplier Inserted Successfully ');
        return redirect('/admin/suppliers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $Supplier)// route model bindding or dependency injection
    {
        return view('admin.suppliers.show',compact('Supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        return view('admin.suppliers.edit',compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);


        $validatedData = $request->validate([
            'company_name' => 'required|min:5',
            'email' => 'required|email',
            'address' => 'required|min:5'
        ]);

        if ($request->hasFile('logo')) {
            $this->unlinkImage($supplier->logo);
            $file = $request->file('logo');
            $validatedData['logo'] = $this->uploadImage($file);
        } else {
            $productData['logo'] = $supplier->logo;
        }


        $Supplier = Supplier::find($id);
        $Supplier->update($validatedData);
        session()->flash('message','Supplier Updated Successfully ');
        return redirect('/admin/suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $this->unlinkImage($supplier->logo);
        $supplier->delete();
        session()->flash('message','Supplier Deleted Successfully ');
        return redirect('/admin/suppliers');
    }

    private function uploadImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable

        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();

        Image::make($file)->resize(300, 200)->save(public_path() . self::UPLOAD_DIR . $image_file_name);

        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }

    private function unlinkImage($img)
    {
         if ($img != '' && file_exists(public_path() . self::UPLOAD_DIR . $img)) {
             @unlink(public_path() . self::UPLOAD_DIR . $img);
         }
    }




}
