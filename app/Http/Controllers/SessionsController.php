<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{

    public  function __construct(){
        return $this->middleware('auth')->except(['create','store']);
    }

    public function create()
    {
        return view('sessions.create');
    }

    public function store(Request $request)
    {



      if(!auth()->attempt(request(['email','password']))){
          return redirect()->back()->withErrors([
              'message'=> 'Please Check Your Credential'
          ]);
      }
       return redirect('/admin');


    }
    public function destroy()
    {
       auth()->logout();
        return redirect('/login');
    }

}
