<?php

namespace App\Http\Controllers;

use App\Notifications\NewOrder;
use App\Notifications\NewProduct;
use App\Order;
use App\Product;
use App\Slider;
use App\User;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home(){

        $products = Product::latest()->get();
        $sliders = Slider::latest()->get();

        return view('index',compact('products','sliders'));

    }
    public function store(Request $request)
    {
        /** @var TYPE_NAME $request */
        $data=$request->only('product_name','number_item','product_price','customer_name','phone_number');
        $order=Order::create($data);


        User::all()->each(function($user) use ($order){
            $user->notify(new NewOrder($order));
        });


        $products = Product::latest()->get();
        $sliders = Slider::latest()->get();
        return view('index',compact('products','sliders'));
    }


    public function showProduct($id){

        $product = Product::find($id);

        return view('single',compact('product'));

    }
}
