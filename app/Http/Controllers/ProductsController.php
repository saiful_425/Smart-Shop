<?php

namespace App\Http\Controllers;

use App\Category;
use App\Notifications\NewProduct;
use App\Product;
use App\Supplier;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    const UPLOAD_DIR = '/uploads/products/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::all();
        return view('admin.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::pluck('name','id');
        $selected_tags = [];
        $category_id = Category::pluck('title','id');
        $supplier_id = Supplier::pluck('company_name','id');

        return view('admin.products.create',compact('tags','selected_tags','category_id','supplier_id'));
//        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productData = $request->only('supplier_id','category_id','name','price','color','description');

        if ($request->hasFile('front_image')) {
            $file = $request->file('front_image');
            $productData['front_image'] = $this->uploadImage($file,'front_image');
        } else {
            $productData['front_image'] = null;
        }

        if ($request->hasFile('back_image')) {
            $file = $request->file('back_image');
            $productData['back_image'] = $this->uploadImage($file,'back_image');
        } else {
            $productData['back_image'] = null;
        }

        //$product = Product::create($productData);
        $product = Product::create($productData);
        $tag_ids = $request->input('tag_ids');
        $product->tags()->attach($tag_ids);

//        $users =User::all();
//
//        foreach ($users as $user)
//        {
//            $user->notify(new NewProduct($product));
//        }


        User::all()->each(function($user) use ($product){
            $user->notify(new NewProduct($product));
        });


        return redirect('/admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show',compact('product'));
    }

    public function addComment(Request $request)
    {
        $product = Product::find($request->product_id);
        $commentData = $request->only('body');
        $product->comments()->create($commentData);
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $tags = Tag::pluck('name','id');
        $selected_tags = [];
        $selected_tags = $product->tags()->pluck('id')->toArray();
        $category_id = Category::pluck('title','id');
        $supplier_id = Supplier::pluck('company_name','id');

        return view('admin.products.edit',compact('product','category_id','supplier_id','tags','selected_tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productData = $request->only('supplier_id','category_id','name','price','color','description');

        $product = Product::find($id);

        if ($request->hasFile('front_image')) {
            $file = $request->file('front_image');
            $productData['front_image'] = $this->uploadImage($file,'front_image');
        } else {
            $productData['front_image'] = $product->front_image;
        }

        if ($request->hasFile('back_image')) {
            $file = $request->file('back_image');
            $productData['back_image'] = $this->uploadImage($file,'back_image');
        } else {
            $productData['back_image'] = $product->back_image;;
        }
        $product->update($productData);
        $tag_ids = $request->input('tag_ids');
        $product->tags()->sync($tag_ids);

//        $users =User::all();
//
//        foreach ($users as $user)
//        {
//            $user->notify(new NewProduct($product));
//        }


        User::all()->each(function($user) use ($product){
            $user->notify(new NewProduct($product));
        });


        return redirect('/admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->unlinkImage($product->image);
        $product->delete();
        return redirect('/admin/products');
    }

    private function uploadImage($file,$image_type = '')
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable

        $image_file_name = $image_type.$timestamp . '.' . $file->getClientOriginalExtension();

        Image::make($file)->resize(250,291)->save(public_path() . self::UPLOAD_DIR . $image_file_name);

        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }


    private function unlinkImage($img)
    {
        if ($img != '' && file_exists(public_path() . self::UPLOAD_DIR . $img)) {
             @unlink(public_path() . self::UPLOAD_DIR . $img);
         }
    }


}
