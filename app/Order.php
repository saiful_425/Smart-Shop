<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable = [
        'product_name','number_item','product_price','customer_name','phone_number'
    ];
}
