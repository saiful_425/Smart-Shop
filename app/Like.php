<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function products()
    {
        return $this->morphedByMany('App\Product', 'likeable');
    }
    protected $fillable = [
        'like',
    ];
}
