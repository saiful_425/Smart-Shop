@extends('admin.layouts.master')

@section('title','Slider List')

@section('page_title','Sliders')

@section('panel_header','Slider List')

@section('content')
    <div class="panel-body">

        @include('admin.layouts.message')

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Slider ID</th>
                <th>Slider Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sliders as $slider)
                <tr>
                    <td>{{$slider->id}}</td>
                    <td ><img src="{{ asset('/uploads/sliders/'.$slider->image) }}" height="150"></td>
                    <td>
                        <a href="{{url('/admin/sliders/'.$slider->id)}}" class="btn btn-info"><i class="fa fa-list"></i> </a>&nbsp;

                        <a href="{{url('/admin/sliders/'.$slider->id.'/edit')}}" class="btn btn-warning"><i class="fa fa-pencil"></i> </a>&nbsp;

                        {!! Form::open(['url' => '/admin/sliders/'.$slider->id,'method' => 'delete']) !!}

                        {!!  Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Slider" />',['type'=>'submit','class'=>'btn btn-danger  pull-left','style'=>'display:inline','onclick'=>'return confirm("Confirm delete?")']) !!}

                        {!!  Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <a href="{{ url('/admin/sliders/create') }}" class="btn btn-info"><i class="fa fa-plus"></i>Add New</a>
    </div>

@endsection

@push('scripts')
        <!-- DataTables JavaScript -->
<script src="{{ asset('back-end/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back-end/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('back-end/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
@endpush
