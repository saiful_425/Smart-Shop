@extends('admin.layouts.master')

@section('title','Slider Show')

@section('page_title','Slider')

@section('panel_header','Slider Show')

@section('content')

    <div class="panel-body">
        <img src="{{ asset('uploads/sliders/'.$slider->image) }}">
    </div>
    <!-- /.panel-body -->

    <div class="panel-footer">
        <a href="{{ url('/admin/sliders') }}" class="btn btn-info">Back</a>
        <a href="{{ url('/admin/sliders/'.$slider->id.'/edit') }}" class="btn btn-warning">Edit</a>
    </div>

@endsection


