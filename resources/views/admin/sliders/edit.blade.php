@extends('admin.layouts.master')

@section('title','Add New Sliders')


@section('page_title','Edit Slider')


@section('panel_header','Slider Edit From')

@section('content')

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">

                {!! Form::open(['url' => ['/admin/sliders',$slider->id],'method' => 'patch','files'=>'true']) !!}

                    <div class="form-group">
                        <img src="{{ asset('/uploads/sliders/'.$slider->image) }}" id="showImage" height="200" alt="" class="img-thumbnail"><br>
                    </div>

                    <div class="form-group">
                        {!! Form::file('image',['class'=>'form-control','id'=>'inputImage']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Update',['class'=>'btn btn-warning','name'=>'btn']) !!}
                    </div>

                {!! Form::close() !!}

            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/sliders') }}" class="btn btn-info">List</a>
    </div>
@endsection

@push('scripts')

<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
    $('#showImage').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#inputImage").change(function(){
    readURL(this);
    });
</script>


@endpush