@extends('admin.layouts.master')

@section('title','Add New Sliders')


@section('page_title','Add Slider')


@section('panel_header','Slider Insert From')

@section('content')

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">
                {!! Form::open(['url' => '/admin/sliders','method' => 'POST','files'=>'true']) !!}

                    <div class="form-group">
                        <img src="{{ asset('/uploads/defaults/slider.jpg') }}" id="showImage" height="200" alt="" class="img-thumbnail"><br>
                    </div>

                    <div class="form-group">
                        {!! Form::file('image',['class'=>'form-control','id'=>'inputImage']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit',['class'=>'btn btn-success','name'=>'btn']) !!}
                    </div>
                {!! Form::close() !!}

            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <div class="panel-footer">
        <a href="{{ url('/admin/sliders') }}" class="btn btn-info"><i class="fa fa-tasks"></i>&nbsp;List</a>
    </div>

@endsection

@push('scripts')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#inputImage").change(function(){
            readURL(this);
        });
    </script>


@endpush
