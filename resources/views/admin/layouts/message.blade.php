<div class="container">
    @if(session()->has('message'))
        <div class="alert alert-success">
            <strong>Success!</strong>
            {{ session('message') }}.
        </div>
    @endif
</div>