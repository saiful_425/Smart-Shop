@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
    <table class="table table-bordered well" style="margin-top: 15px;">
        <thead class="text-center">
        <tr>
            <th>Product Name</th>
            <th>Number Of Item</th>
            <th>Customer Name</th>
            <th>Mobile Numebr</th>
            <th>Order Time</th>
            <th>Action</th>
        </tr>
        </thead>
        @foreach($orders as $order)
            <tbody>
            <tr>
                <td>{{$order->product_name}}</td>
                <td>{{$order->number_item}}</td>
                <td>{{$order->customer_name}}</td>
                <td>{{$order->phone_number}}</td>
                <td>{{$order->created_at }}</td>
                {{--<td><a href="{{url()}}" class="btn btn-danger">Delete</a></td>--}}
                <td> {!! Form::open([
                                   'method'=>'DELETE',
                                   'url' => ['admin/oreder/delete', $order->id],
                                   'style' => 'display:inline'
                                   ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Article" />', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Article',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}</td>
            </tr>
            </tbody>
        @endforeach
    </table>
    </div>
</div>
    @stop