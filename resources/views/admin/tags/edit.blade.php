
@extends('admin.layouts.master')

@section('title','Edit Tag')


@section('page_title','Edit Tag')


@section('panel_header','Tag Edit From')

@section('content')
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => ['/admin/tags',$tag->id],'method' => 'patch']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name',$tag->name,['class'=>'form-control','placeholder'=>'Enter the name']) !!}
                </div>

                <div class="form-group">
                     {!! Form::submit('update',['class'=>'btn btn-warning','name'=>'btn']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    <!-- /.row (nested) -->
    </div>
    <div class="panel-footer">
        <a href="{{ url('/admin/tags') }}" class="btn btn-info"><i class="fa fa-tasks"></i>&nbsp; List</a>
    </div>
@endsection