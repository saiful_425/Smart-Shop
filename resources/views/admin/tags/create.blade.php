
@extends('admin.layouts.master')

@section('title','Add New Tag')


@section('page_title','Add New Tag')


@section('panel_header','Tag Insert From')

@section('content')

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">
                {!! Form::open(['url' => '/admin/tags','method' => 'POST']) !!}

                <div class="col-xs-4">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter tag name']) !!}

                <br>
                {!! Form::submit('Submit',['class'=>'btn btn-success','name'=>'btn']) !!}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
@endsection
