@extends('admin.layouts.master')

@section('title','Tag List')

@section('page_title','Tags')

@section('panel_header','Tag List')

@section('content')

    <div class="panel-body">

        @include('admin.layouts.message')

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>SL.</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $sl = 0;
            @endphp
            @foreach($tags as $tag)
                <tr class="odd gradeX">
                    <td>{{ ++$sl }}</td>
                    <td>{{$tag->name}}</td>
                    <td>
                        <a href="{{url('/admin/tags/'.$tag->id)}}" class="btn btn-success">Show</a>

                        <a href="{{url('/admin/tags/'.$tag->id.'/edit')}}" class="btn btn-info">Edit</a>

                        {!! Form::open(['url' => '/admin/tags/'.$tag->id,'method' => 'delete']) !!}

                        {{ Form::submit('delete',['class'=>'btn btn-danger','name'=>'btn']) }}

                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/tags/create') }}" class="btn btn-info"><i class="fa fa-plus"></i>Add New</a>
    </div>

@endsection

@push('scripts')
    <!-- DataTables JavaScript -->
    <script src="{{ asset('back-end/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
@endpush