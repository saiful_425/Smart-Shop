
@extends('admin.layouts.master')

@section('title','Show A Tag')


@section('page_title','Show A Tag')


@section('panel_header','Tag Show From')

@section('content')

<div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <th>Tags</th>
            <th>Action</th>
        </tr>
        </thead>
            <tbody>
            <tr>
                <td>{{$tag->name}}</td>
                <td>
                    <a href="{{url('/admin/tags')}}" class="btn btn-info">Back</a>
                </td>
            </tr>
            </tbody>
    </table>
</div>

<div class="panel-footer">
    <a href="{{ url('/admin/tags') }}" class="btn btn-info">List</a>
    <a href="{{ url('/admin/tags/'.$tag->id.'/edit') }}" class="btn btn-warning">Edit</a>
</div>
@endsection