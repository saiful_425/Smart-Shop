@extends('admin.layouts.master')

@section('title','Edit Product')

@section('page_title','Product')

@section('panel_header','Product Edit From')

@section('content')

    <div class="panel-body">
        <div class="row">

            <div class="col-md-10 col-md-offset-1">

                {!! Form::open(['url' => ['/admin/products',$product->id],'method' => 'patch']) !!}

                <div class="form-group">
                    {!! Form::label('supplier_id', 'Supplier:') !!}
                    {!! Form::select('supplier_id',$supplier_id,$product->supplier_id,['placeholder' => 'Select One ...','required' => 'required','class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('category_id', 'Category:') !!}
                    {!! Form::select('category_id',$category_id,$product->category_id,['placeholder' => 'Select One ...','required' => 'required','class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name',$product->name,['class'=>'form-control','placeholder'=>'Enter Name','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'Price:') !!}
                    {!! Form::number('price',$product->price,['class'=>'form-control','placeholder'=>'Enter price','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('color', 'Color:') !!}
                    {!! Form::text('color',$product->color,['class'=>'form-control','required' => 'required','placeholder'=>'Enter color']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('front_image', 'Front Image:') !!}
                    {!! Form::file('front_image',['class'=>'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('back_image', 'Back Image:') !!}
                    {!! Form::file('back_image',['class'=>'form-control']) !!}
                </div>
                {{--checkbox--}}
                <div class="form-group">
                    {!! Form::label('Description', 'Description:') !!}
                    {!! Form::textarea('description',$product->description,['placeholder'=>'Enter Description','id'=>'description','class'=>'form-control','required' => 'required']) !!}
                </div>
                {{--checkbox--}}

                <div class="form-group">
                    {!! Form::label('tag_ids', 'Tags:') !!}
                    <div class="bg-info clearfix">
                        @foreach ($tags as $id => $name)
                            <label>
                                {!! Form::checkbox('tag_ids[]', $id, in_array($id, $selected_tags)) !!}
                                {{ $name }}
                            </label>
                        @endforeach
                    </div>
                </div>
                {{--checkbox--}}

                <div class="form-group">
                    {!! Form::submit('Update',['class'=>'btn btn-warning','name'=>'btn']) !!}
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/products') }}" class="btn btn-info">List</a>
    </div>
@endsection

@push('scripts')
    <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
    {{--<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>--}}
    <script>
        CKEDITOR.replace( 'description' );
    </script>
@endpush
