@extends('admin.layouts.master')

@section('title','Product Show')

@section('page_title','Product')

@section('panel_header','Product Show')

@section('content')

    <div class="panel-body">
            <img src="{{ asset('/uploads/products/'.$product->front_image) }}">
            <img src="{{ asset('/uploads/products/'.$product->back_image) }}">
            <h3>Name  : {{$product->name}}</h3>
            <p>Category  : {{$product->category->title}}</p>
            <p>Supplier  : {{$product->supplier->company_name}}</p>
            <p>Price : {{$product->price}}</p>
            <p>Color : {{$product->color}}</p>
            <p>Description : {!! $product->description !!}</p>
            <p>Tags :
                @foreach($product->tags as $tag)
                {!! $tag->name !!}&nbsp;&nbsp;
                @endforeach
            </p>
            <p>Created at : {!! $product->created_at !!}</p>
            <p>updated at : {!! $product->updated_at !!}</p>

    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/products') }}" class="btn btn-info">List</a>
        <a href="{{ url('/admin/products/'.$product->id.'/edit') }}" class="btn btn-warning">Edit</a>
    </div>
@endsection