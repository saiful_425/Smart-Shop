@extends('admin.layouts.master')

@section('title','Category List')

@section('page_title','Products')

@section('panel_header','Products List')

@section('content')

    <div class="panel-body">

        @include('admin.layouts.message')

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Supplier</th>
                <th>Category</th>
                <th>Name</th>
                <th>price</th>
                <th>Color</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
             <tr class="odd gradeX">
                <td>{{$product->supplier->company_name}}</td>
                <td>{{$product->category->title}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->color}}</td>
                <td>
                    <a href="{{url('/admin/products/'.$product->id)}}">Show</a>

                    <a href="{{url('/admin/products/'.$product->id.'/edit')}}">Edit</a>

                    {!! Form::open(['url' => '/admin/products/'.$product->id,'method' => 'delete']) !!}

                    {!!  Form::submit('delete',['class'=>'btn btn-danger','name'=>'btn']) !!}

                    {!!  Form::close() !!}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/products/create') }}" class="btn btn-info"><i class="fa fa-plus"></i>Add New</a>
    </div>

@endsection

@push('scripts')
    <!-- DataTables JavaScript -->
    <script src="{{ asset('back-end/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
@endpush