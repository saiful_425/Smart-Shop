@extends('admin.layouts.master')

@section('title','Supplier Create')


@section('page_title','Suppliers')

@section('panel_header','Supplier Create Form')


@section('content')
    <div class="panel-body">

        <p>Company Name : {{$Supplier->company_name}}</p>
        <br>
        <img src="{{ asset('/uploads/suppliers/logo/'.$Supplier->logo) }}">
        <br>
        <p>Email : {{$Supplier->email}}</p>
        <br>
        <p>Address : {{$Supplier->address}}</p>


            <!-- /.col-lg-6 (nested) -->
    </div>
    <div class="panel-footer">
        <a href="{{ url('/admin/suppliers') }}" class="btn btn-info">List</a>
        <a href="{{ url('/admin/suppliers/'.$Supplier->id.'/edit') }}" class="btn btn-warning">Edit</a>
    </div>
        <!-- /.row (nested) -->
@endsection
