@extends('admin.layouts.master')

@section('title','Supplier Create')


@section('page_title','Suppliers')

@section('panel_header','Supplier Create Form')


@section('content')
<div class="panel-body">
    @foreach ($errors->all() as $message)
        {{ $message }}
    @endforeach
    <div class="row">
        <div class="col-lg-6">
            {!! Form::open(['url' => 'admin/suppliers','method'=>'post','files'=>true]) !!}

                <div class="form-group">
                    {{ Form::label('Company Name') }}
                    {{ Form::text('company_name',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Company Logo') }}
                    {{ Form::file('logo',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Email') }}
                    {{ Form::email('email',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Address') }}
                    {{ Form::textarea('address',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Add',['class'=>'btn btn-md btn-primary']) }}
                </div>
            {!! Form::close() !!}

        </div>
        <!-- /.col-lg-6 (nested) -->
    </div>
    <!-- /.row (nested) -->
</div>
@endsection
