@extends('admin.layouts.master')

@section('title','Supplier List')

@section('page_title','Suppliers')

@section('panel_header','Supplier List')

@section('content')

    <div class="panel-body">

        @include('admin.layouts.message')

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>SL.</th>
                <th>Company</th>
                <th>lOGO</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach($suppliers as $supplier)
                    <tr class="gradeU">
                        <td>{{ ++$sl }}</td>
                        <td >{{ $supplier->company_name }}</td>
                        <td ><img src="{{ asset('/uploads/suppliers/logo/'.$supplier->logo) }}" height="150"></td>
                        <td >{{ $supplier->email }}</td>
                        <td >{{ $supplier->address }}</td>
                        <td >
                            <a href="{{ url('/admin/suppliers/'.$supplier->id) }}" class="btn btn-success">Show</a>
                            <a href="{{ url('/admin/suppliers/'.$supplier->id.'/edit') }}" class="btn btn-info">Edit</a>

                            {!! Form::open(['url' => '/admin/suppliers/'.$supplier->id, 'method'=>'delete']) !!}
                                {{ Form::submit('Delete',['class'=>'btn btn-danger','onclick'=>'return confirm("Confirm delete?")']) }}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <a href="{{ url('/admin/suppliers/create') }}" class="btn btn-info"><i class="fa fa-plus"></i>Add New</a>
    </div>

@endsection

@push('scripts')
    <!-- DataTables JavaScript -->
    <script src="{{ asset('back-end/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
@endpush

