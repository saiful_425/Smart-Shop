@extends('admin.layouts.master')

@section('title','Supplier Edit')


@section('page_title','Suppliers')

@section('panel_header','Supplier Edit Form')


@section('content')
    <div class="panel-body">
        @foreach ($errors->all() as $message)
            {{ $message }}
        @endforeach
        <div class="row">
            <div class="col-lg-6">
                {!! Form::open(['url' => ['admin/suppliers',$supplier->id],'method'=>'patch','files'=>true]) !!}

                <div class="form-group">
                    {{ Form::label('Company Name') }}
                    {{ Form::text('company_name',$supplier->company_name,['class'=>'form-control']) }}
                </div>

                <img src="{{ asset('/uploads/suppliers/logo/'.$supplier->logo) }}" height="150" alt="">

                <div class="form-group">
                    {{ Form::label('Company Logo') }}
                    {{ Form::file('logo',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Email') }}
                    {{ Form::email('email',$supplier->email,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('Address') }}
                    {{ Form::textarea('address',$supplier->address,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Update',['class'=>'btn btn-md btn-primary']) }}
                </div>
                {!! Form::close() !!}

            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
    </div>
@endsection
