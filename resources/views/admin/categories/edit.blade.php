@extends('admin.layouts.master')

@section('title','Category Edit')


@section('page_title','Category')

@section('panel_header','Category Edit Form')


@section('content')
    <div class="panel-body">
        @foreach ($errors->all() as $message)
            {{ $message }}
        @endforeach
        <div class="row">
            <div class="col-lg-6">
                {!! Form::open(['url' => 'admin/categories/'.$category->id,'method'=>'patch']) !!}
                <div class="form-group">
                    {{--<label></label>--}}
                    {{ Form::label('Category Name') }}
                    {{--<input name="name" class="form-control">--}}
                    {{ Form::text('name',$category->title,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{--<button type="submit" class="btn btn-md btn-primary">Add</button>--}}
                    {{ Form::submit('Update',['class'=>'btn btn-md btn-primary']) }}
                </div>
                {!! Form::close() !!}

            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
    </div>
@endsection
