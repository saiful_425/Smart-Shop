@extends('admin.layouts.master')

@section('title','Category Create')


@section('page_title','Category')

@section('panel_header','Category Create Form')


@section('content')
<div class="panel-body">
    @foreach ($errors->all() as $message)
        {{ $message }}
    @endforeach
    <div class="row">
        <div class="col-lg-6">
            {!! Form::open(['url' => 'admin/categories','method'=>'post']) !!}
                <div class="form-group">
                    {{--<label></label>--}}
                    {{ Form::label('Category Title') }}
                    {{--<input name="name" class="form-control">--}}
                    {{ Form::text('title',null,['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{--<button type="submit" class="btn btn-md btn-primary">Add</button>--}}
                    {{ Form::submit('Add',['class'=>'btn btn-md btn-primary']) }}
                </div>
            {!! Form::close() !!}

        </div>
        <!-- /.col-lg-6 (nested) -->
    </div>
    <!-- /.row (nested) -->
</div>
@endsection
