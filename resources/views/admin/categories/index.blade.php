@extends('admin.layouts.master')

@section('title','Category List')

@section('page_title','Category')

@section('panel_header','Category List')

@section('content')

    <div class="panel-body">

        @include('admin.layouts.message')

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>SL.</th>
                <th>Category</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach($categories as $category)
                    <tr class="gradeU">
                        <td>{{ ++$sl }}</td>
                        <td >{{ $category->title }}</td>
                        <td >
                            <a href="{{ url('/admin/categories/'.$category->id) }}">Show</a>
                            <a href="{{ url('/admin/categories/'.$category->id.'/edit') }}">Edit</a>

                            {!! Form::open(['url' => '/admin/categories/'.$category->id, 'method'=>'delete']) !!}
                                {{ Form::submit('Delete',['onclick'=>'return confirm("Confirm delete?")']) }}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->

@endsection

@push('scripts')

    <!-- DataTables JavaScript -->
    <script src="{{ asset('back-end/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('back-end/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
@endpush

